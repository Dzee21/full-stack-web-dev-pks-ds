<?php

//class hewan
trait hewan
{

    //property
    public  $nama,
        $darah = '50',
        $jumlahkaki,
        $keahlian;

    public function atraksi()
    {
        echo "{$this->nama} sedang {$this->keahlian}";
    }
}

//class fight   
abstract class fight
{
    use hewan;
    //property
    public $attackpower = '';
    public $defencepower = '';

    public function serang($hewan)
    {
        echo "{$this->nama} sedang menyerang {$hewan->nama}";
        echo "<br>";
        $hewan->diserang($this);
    }

    public function diserang($hewan)
    {
        echo "{$this->nama} sedang diserang {$hewan->nama}";
        $this->darah = ($this->darah - $this->attackpower / $this->defencepower);
    }

    protected function getinfo()
    {
        echo "Nama : {$this->nama}";
        echo "<br>";
        echo "Jumlah Kaki : {$this->jumlahkaki}";
        echo "<br>";
        echo "Keahlian = {$this->keahlian}";
        echo "<br>";
        echo "Darah = {$this->darah}";
        echo "<br>";
        echo "Attack Power  = {$this->attackpower}";
        echo "<br>";
        echo "Defence Power  = {$this->defencepower}";
    }
}

class elang extends fight
{

    public function __construct($nama)
    {

        $this->nama = $nama;
        $this->jumlahkaki = 2;
        $this->keahlian = 'terbang tinggi';
        $this->attackpower = 10;
        $this->defencepower = 5;
    }

    public function getinfohewan()
    {
        echo "jenis hewan : Elang";
        echo "<br>";

        $this->getinfo();
    }
}

class harimau extends fight
{

    public function __construct($nama)
    {

        $this->nama = $nama;
        $this->jumlahkaki = 4;
        $this->keahlian = 'lari cepat';
        $this->attackpower = 7;
        $this->defencepower = 8;
    }

    public function getinfohewan()
    {
        echo "jenis hewan : Harimau";
        echo "<br>";

        $this->getinfo();
    }
}

class spasi
{
    public static function space()
    {
        echo "<br>";
        echo "=============";
        echo "<br>";
    }
}


$harimau = new harimau("Harimau Jawa");
$harimau->getinfohewan();
spasi::space();
$elang = new elang("Elang Papua");
$elang->getinfohewan();
spasi::space();
$harimau->serang($elang);
spasi::space();
$elang->getinfohewan();
spasi::space();
$elang->serang($harimau);
spasi::space();
$harimau->getinfohewan();
